const express = require("express");
const app = express();
const http = require("http");
const cors = require("cors");
const { Server } = require("socket.io");
app.use(cors());

const server = http.createServer(app);

const io = new Server(server, {
    cors: {
        // origin: "http://127.0.0.1:5173",
        origins: ["http://localhost:*", "http://127.0.0.1:*"]
    },
});

io.on("connection", (socket) => {
    console.log(`User Connected: ${socket.id}`);

    socket.on("join_room", (data) => {
        socket.join(data);
        console.log(`User with ID: ${socket.id} joined room: ${data}`);
    });

    socket.on("send_message", (data) => {
        socket.broadcast.to(data.room).emit("receive_message", data)
    });

    socket.on("disconnect", () => {
        console.log("User Disconnected", socket.id);
    });
});

app.get('/', (res, req) => {
    console.log('.............');
})

server.listen(3001, () => {
    console.log("SERVER RUNNING in 3001");
    // socket.to(data.room).emit("receive_message", data);
});

//...............old code................

// const express = require("express");
// const app = express();
// const http = require("http");
// // const server = http.createServer(app);
// // const socket = require("socket.io");
// // const io = socket(app);
// const cors = require('cors')
// app.use(express.json())
// app.use(express.urlencoded({ extended: true }))
// app.use(cors({
//     origin: ["http://localhost:8007", "http://localhost:9955"],
//     credentials: true,
// }))


// const mySocket = app.listen(9955, () => {
//     console.log("server is running on port 9955")
// });

// app.get('/', (req, res) => {
//     console.log(res, req, '........................\n\n........')
// })

// const { Server } = require("socket.io");
// const server = http.createServer(app);

// const io = new Server(server, {
//     cors: {
//         origin: 'http://127.0.0.1:5173',
//         methods: ['GET', 'POST']
//     }
// })
// // io.on("connection", socket => {
// //     socket.emit("your id", socket.id);
// //     socket.on("send message", body => {
// //         io.broadcast.emit("message", body)
// //     })
// //     // socket.on('join', function (data) {
// //     //     socket.join(data.email); // We are using room of socket io
// //     // });
// // })

// // const io = require('socket.io')(mySocket)

// io.on('connection', socket => {
//     const id = socket.handshake.query.id
//     socket.join(id)
//     console.log(id)

//     socket.on('send-message', ({ recipients, text }) => {
//         recipients.forEach(recipient => {
//             const newRecipients = recipients.filter(r => r !== recipient)
//             newRecipients.push(id)
//             socket.broadcast.to(recipient).emit('receive-message', {
//                 recipients: newRecipients, sender: id, text
//             })
//         })
//     })
// })

// // mongodb+srv://gayatri31prasad:Gayatri@3101@cluster0.kxkkepd.mongodb.net/MyDBName?retryWrites=true&w=majority